CREATE DATABASE bikedb;
USE bikedb;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(100) DEFAULT NULL,
  `userEmail` varchar(100) DEFAULT NULL,
  `userPass` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `user` VALUES ('Peter','peter@peter.de','peter1234');

