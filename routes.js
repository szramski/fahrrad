//import web server app
let app = require('./app');


//import mysql db connection
let con = require('./mysqlcon');

con.connect( err => {
    if (err) throw err;
    console.log("Connected to DB");
  });

//root route:
app.get('/', (req, res) => {
    //console.log("User ID: " + req.session.user_id);
    res.render("index")
})

app.get('/bicicletas', (req, res) => {
    //console.log("User ID: " + req.session.user_id);
    res.render("bicicletas")
})

app.get('/login', (req, res) => {
    //console.log("User ID: " + req.session.user_id);
    res.render("login")
})

app.get('/registro', (req, res) => {
    //console.log("User ID: " + req.session.user_id);
    res.render("registro")
})


app.post('/addUser', (req, res) => {
    
    let nuevoUsuario = req.body;

    let sql = `INSERT INTO user(
                userName, 
                userEmail, 
                userPass) 
                VALUES (
                    '${nuevoUsuario.userName}',
                    '${nuevoUsuario.userEmail}',
                    '${nuevoUsuario.userPass}'
                )`;

    con.query(sql, nuevoUsuario, (err, result) => {
        if(err){
            console.log("Error al insertar: " + err.code);
            } else {
            res.send(
                { "status": "OK"}
            );
        }
        
    });
    
});

app.post('/login', (req, res) => {

    let usuario = req.body;

    let sql = `SELECT * 
                FROM user 
                WHERE userEmail = '${ usuario.userEmail  }'
                AND userPass = '${ usuario.userPass  }' `;
    

    con.query(sql, (err, result) => {
        if(result.length != 0){
            
        if(!req.session.user_id){
                req.session.userName = result[0].userName;
        }
            res.status(201).send(req.session)
        } else {
            res.send(
                { "status": "FALSE"}
            );            
        }//else
    });

});

app.get('/admin', (req, res)=> {
    res.render('admin')
})

app.get('/admin/users/getAll', (req, res) => {

    let sql = 'select * from user';

    con.query(sql, (err, result)=>{

        if(err) throw err;
       
        res.send(result);
    })//query

})

app.post('/admin/users/update', (req, res) => {

    let sql = `UPDATE user 
               SET userName='${req.body.updatedUserName}', 
               userEmail='${req.body.updatedUserEmail}' 
               WHERE id=${req.body.userId}`;

    con.query(sql, (err, result)=>{

        if(err) throw err;
       
        sql = `SELECT * FROM user WHERE id=${req.body.userId}`;

        con.query(sql, (err, result) => {
            if (err) throw err;

            res.send(result);

         })//query

    })//query

})


app.post('/admin/users/delete', (req, res) => {

    let sql = `DELETE FROM user 
               WHERE id=${req.body.userId}`;
          
    con.query(sql, (err, result)=>{

        res.send(
            { "status": "OK"}
        );          
    })//query


})