$('document').ready( () => {

    //save all buttons and inputs in variables:
    let btn_registro_cancelar = $('#btn-registro-cancelar');
    let btn_registro_iniciar = $('#btn-registro-iniciar');

    let iptUserName = $('#ipt-registro-nombre');
    let iptUserEmail = $('#ipt-registro-email');
    let iptUserPass1 = $('#ipt-registro-password');
    let iptUserPass2 = $('#ipt-registro-rpassword');

    let divInvalidFeedbackNombre = $('#invalid-feedback-nombre');
    let divInvalidFeedbackEmail = $('#invalid-feedback-email');  
    let divInvalidFeedbackPassword = $('#invalid-feedback-password'); 
    let divInvalidFeedbackRpassword = $('#invalid-feedback-rpassword');  

    //hide error message divs (initial state)
    divInvalidFeedbackNombre.addClass('hideInvalidFeedback'); 
    divInvalidFeedbackEmail.addClass('hideInvalidFeedback');  
    divInvalidFeedbackPassword.addClass('hideInvalidFeedback'); 
    divInvalidFeedbackRpassword.addClass('hideInvalidFeedback');     
    

    /* ========== Handle Click on Cancel Btn ==================== */

    btn_registro_cancelar.click( () => {
        $(location).attr('href', '/bicicletas')
    })//cancelar click


    /* ========== Handle Click on Register Btn ==================== */
    
    btn_registro_iniciar.click( () => {

    //hide error message divs
    divInvalidFeedbackNombre.addClass('hideInvalidFeedback'); 
    divInvalidFeedbackEmail.addClass('hideInvalidFeedback');  
    divInvalidFeedbackPassword.addClass('hideInvalidFeedback'); 
    divInvalidFeedbackRpassword.addClass('hideInvalidFeedback');  

        //remove red border around the text inputs
        if ( iptUserName.hasClass('redBorder') ) {
             iptUserName.removeClass('redBorder');
        }
        if ( iptUserEmail.hasClass('redBorder') ) {
             iptUserEmail.removeClass('redBorder');
        }
        if ( iptUserPass1.hasClass('redBorder') ) {
             iptUserPass1.removeClass('redBorder');
        }
        if ( iptUserPass2.hasClass('redBorder') ) {
             iptUserPass2.removeClass('redBorder');
        }        

        //get values from inputs
        let userName = iptUserName.val();
        let userEmail = iptUserEmail.val();
        let userPass1 = iptUserPass1.val();
        let userPass2 = iptUserPass2.val();

        //control variable that allows to send data in case that everything's fine:
        let okParaEnviar = true;

        //value check:
        if (userName === "" ){
            iptUserName.addClass('redBorder');
            divInvalidFeedbackNombre.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 

        if (userEmail === "" || !userEmail.includes("@") || !userEmail.includes(".") ){
            iptUserEmail.addClass('redBorder');
            divInvalidFeedbackEmail.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 

        if (userPass1 === "" || userPass1.length < 4){
            iptUserPass1.addClass('redBorder');
            divInvalidFeedbackPassword.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 

        if (userPass1 === "" || userPass2 != userPass1 ){
            iptUserPass2.addClass('redBorder');
            divInvalidFeedbackRpassword.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 

        if (okParaEnviar){


            let newUser = { userName, userEmail, userPass: userPass1 };
            
            $.post('http://127.0.0.1:3000/addUser', newUser, (resultado) => {
                console.log("Cliente ha recibido: ", resultado);
                if(resultado.status === "OK"){
                    alert("Se ha registrado con éxito. Abriendo la página de login");
                    $(location).attr('href', '/login')
                }
            })//post

        }//if okParaEnviar
        
    })//enviar click

    /* ============================== */

});