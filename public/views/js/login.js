
$('document').ready( () => {

    let btn_login_cancelar = $('#btn-login-cancelar');
    let btn_login_iniciar = $('#btn-login-iniciar');


    //hiding error message divs
    $('#invalid-feedback-email').addClass('hideInvalidFeedback'); 
    $('#invalid-feedback-password').addClass('hideInvalidFeedback');    

    btn_login_cancelar.on('focus', function(){
        $(this).val("");
    })

    btn_login_cancelar.click( () => {
        $(location).attr('href', '/bicicletas')
    })//cancelar click


    
    btn_login_iniciar.click( () => {

        let iptLoginEmail = $('#ipt-login-email');
        let iptLoginPassword = $('#ipt-login-password');
        let iptInvalidFeedbackEmail = $('#invalid-feedback-email');
        let iptInvalidFeedbackPassword = $('#invalid-feedback-password');
        

        //remove red border around the text inputs
        if ( iptLoginEmail.hasClass('redBorder') ) {
            iptLoginEmail.removeClass('redBorder');
        }
        if ( iptLoginPassword.hasClass('redBorder') ) {
            iptLoginPassword.removeClass('redBorder');
        }

        $('#invalid-feedback-email').addClass('hideInvalidFeedback'); 
        $('#invalid-feedback-password').addClass('hideInvalidFeedback'); 


           
        let userEmail = iptLoginEmail.val();
        let userPass = iptLoginPassword.val();

        let okParaEnviar = true;

        if (userEmail === "" || !userEmail.includes("@") || !userEmail.includes(".") ){
            //alert("Indica tu email");
            iptLoginEmail.addClass('redBorder');
            iptInvalidFeedbackEmail.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 
        
        if  (userPass === "" || userPass.length < 4){
            //alert("Indica tu contraseña");
            iptLoginPassword.addClass('redBorder');
            iptInvalidFeedbackPassword.removeClass('hideInvalidFeedback');
            okParaEnviar = false;
        } 
        
        if (okParaEnviar){

            let newLogin = { userEmail, userPass };
            console.log(newLogin);
            
            $.post('http://127.0.0.1:3000/login', newLogin, (resultado) => {
                 
                if(resultado.status === "FALSE"){
                    alert("Este usuario no existe. Regìstrate.");
                } else {
                    iptInvalidFeedbackEmail.addClass('hideInvalidFeedback');
                    iptInvalidFeedbackPassword.addClass('hideInvalidFeedback');

                    alert("Hola, " + resultado.userName);
                    //$(location).attr('href', 'http://localhost:3000/bicicletas')
                }
                
            })//post 
            


        }//else
    })//enviar click
})//ready

