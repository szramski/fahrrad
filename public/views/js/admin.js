$('document').ready(() => {

    //Primero miramos si el administrador está logeado
    // ... por hacer más tarde

    //Mostramos todos los usuarios:
    $.get('/admin/users/getAll', (userList) => {

        if(userList.length != 0){

            userList.map( (user) => {

                $('tbody').append(`
                <tr>
                    <th scope="row">${user.id}</th>
                    <td><input type="text" id="iptUserName" value="${user.userName}"></input></td>
                    <td><input type="text" id="iptUserEmail" value="${user.userEmail}"></td>
                    <td><button id="btnActualizarUsuario" data-userid="${user.id}" class="btn-warning">Actualizar</button></td>
                    <td><i class="fas fa-check text-success operacion-ok-hidden"></i></td>                
                    <td><button id="btnBorrarUsuario" data-userid="${user.id}" class="btn-danger">Borrar</button></td>
              </tr>
                `)//append
    
            });//map
        } else {
                $('tbody').append(`
                <tr>
                    <td colspan="6">No existen usuarios en la base de datos.</td>
                </tr>`);
        }


    })//get all users

 /***************************/ 
    
    $('tbody').on('click', '#btnBorrarUsuario',function(){
        
        let userId = $(this).data('userid');

        $.post('http://localhost:3000/admin/users/delete', {userId}, (res)=>{

        if(res.status === "OK"){
           let currentTR =  $(this).parent().parent();
           //the current TR will kill itself ;)
           currentTR.remove();
        }

        });//post

    });//tbody on click btnBorrarUsuario

 /***************************/

    $('tbody').on('click', '#btnActualizarUsuario',function(){
        
        let userId = $(this).data('userid');
        let updatedUserName = $(this).parent().parent().find('#iptUserName').val();
        let updatedUserEmail = $(this).parent().parent().find('#iptUserEmail').val();

        let updatedUserData = { userId, updatedUserName,  updatedUserEmail};

        $.post('http://localhost:3000/admin/users/update', updatedUserData, (res)=>{

            //si el update va bien, el servidor nos envia un array con el resultado
            //de select de este mismo usuario. Este resultado metemos en los inputs:
            
            $(this).parent().parent().find('#iptUserName').val(res[0].userName)
            $(this).parent().parent().find('#iptUserEmail').val(res[0].userEmail)


            $(this).parent().parent().find('i').removeClass('operacion-ok-hidden');

            setTimeout(() => {

                $(this).parent().parent().find('i').addClass('operacion-ok-hidden');
                
            }, 1000); //setTimeout

        })//post


    });//tbody on click






  


});//ready